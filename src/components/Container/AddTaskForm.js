import React from "react";

export default function AddTaskForm({
task,
    onAddFormSubmit,
    onAddInputChange
  }) {
    return (
      <form onSubmit={onAddFormSubmit}>
        <label htmlFor="task">Create Task: </label>
        <input
          name="task"
          type="text"
          placeholder="Create new task"
          value={task}
          onChange={onAddInputChange}
        />
      </form>
    );
  }
  