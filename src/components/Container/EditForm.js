import React from "react"

export default function EditForm({
    currentTodo,
    setIsEditing,
    onEditInputChange,
    onEditFormSubmit
  }) {
    return (
      <form onSubmit={onEditFormSubmit}>
        <h2>Edit Task</h2>
        <label htmlFor="updateTask">Update Task: </label>
        <input
          name="updateTask"
          type="text"
          placeholder="Update task"
          value={currentTodo.text}
          onChange={onEditInputChange}
        />
        <button type="submit" onClick={onEditFormSubmit}>
          Update
        </button>
        <button onClick={() => setIsEditing(false)}>Cancel</button>
      </form>
    );
  }