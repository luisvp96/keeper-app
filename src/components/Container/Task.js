import React from "react";

export default function Task({
    task,
    onEditClick,
    onDeleteClick
  }) {
    return (
      <li key={task.id}>
        {task.text}
        <button onClick={() => onEditClick(task)}>Edit</button>
        <button onClick={() => onDeleteClick(task.id)}>Delete</button>
      </li>
    );
  }